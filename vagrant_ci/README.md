# vagrant_ci


## Overview

Based HEAVILY on https://gitlab.oit.duke.edu/jnt6/singularity-build-vagrant (thanks @jnt@duke.edu))

## Usage

Check this git repository out on your gitlab-ci host.  Install all of the
necessary stuff listed in the repository above

Run:

```
./reset_build_vm.sh
```

Configure gitlab runner to use the new VirtualBox instance

`/etc/gitlab-runner/config.toml`

```
concurrent = 4
check_interval = 0

[[runners]]
  name = "vagrant_ci"
  url = "https://gitlab.oit.duke.edu/"
  token = "your_token"
  executor = "virtualbox"
  [runners.ssh]
    user = "vagrant"
    identity_file = "/srv/vagrant_ci/.vagrant/machines/default/virtualbox/private_key"
  [runners.virtualbox]
    base_name = "vagrant_ci"
    disable_snapshots = false
  [runners.cache]
```
