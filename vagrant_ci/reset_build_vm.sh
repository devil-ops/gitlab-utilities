#!/bin/bash

if [ ! -f Vagrantfile ]; then
    echo "No Vagrantfile found, please ensure you are in the right directory"
    exit 1
fi
# this just nukes everything from orbit and tries again
killall -9 VBoxHeadless 2>/dev/null
sleep 4
killall -9 VBoxHeadless 2>/dev/null


vagrant plugin list | grep vagrant-disksize &>/dev/null || vagrant plugin install vagrant-disksize
rm -fr "${HOME}/VirtualBox VMs/$(basename "$PWD")"* .vagrant/ 
export SP_VERSION=$(rpm -q gitlab-runner --qf "%{VERSION}\n" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
vagrant up
vagrant halt
