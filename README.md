# Overview

Place for shared tools like CI triggers and such

## ci/

Misc. files that can be included in your `gitlab-ci.yml` with lines like:

```yaml
include: 'https://gitlab.oit.duke.edu/devil-ops/gitlab-utilities/raw/master/ci/code_quality.yaml'
```

## vagrant_ci/

This is the vagrant information used to generate the VirtualBox image that will run the CI bits
